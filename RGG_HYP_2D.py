import numpy as np
import networkx as nx
import random
import matplotlib.pyplot as plt

R=4E-3
num_vertex=1000

def pair_within_R(vertex1_index, vertex2_index, radius, graph):
    """
    Tests whether two vertices are within hyperbolic distance R of another and are causally related


    Returns True if vertices within R and causally related, False if not

    args:
    vertex1_index, vertex2_index -- the indices of the two nodes to be tested - type int
    radius -- hyperbolic radius which t^2-x^2 must be less than - type float
    graph -- graph to be tested - type Networkx Graph with time, xpos as node attributes
    """
    tattribs=nx.get_node_attributes(graph, 'time')
    xattribs=nx.get_node_attributes(graph,'xpos')
    yattribs=nx.get_node_attributes(graph,'ypos')
    tau = (tattribs[vertex1_index]-tattribs[vertex2_index])**2 - \
    (xattribs[vertex1_index]-xattribs[vertex2_index])**2 - (yattribs[vertex1_index]-yattribs[vertex2_index])**2
    if 0 < tau <= radius:
        return True
    else:
        return False


def temporal_order(vertex1_index, vertex2_index,graph):
    """
    Returns two input vertex indices in the order they occur temporally - type int, int

    args:
    vertex1_index, vertex2_index -- indices of the two nodes to be tested - type int
    graph -- graph to be tested - type Networkx Graph with time, xpos as node attributes
    """
    tattribs=nx.get_node_attributes(graph, 'time')
    if tattribs[vertex1_index] < tattribs[vertex2_index]:
        return vertex1_index, vertex2_index
    else:
        return vertex2_index, vertex1_index

def complete_successor_tree(graph, start_vertex):
    """
    Generates the complete successor tree for a given vertex in the graph

    Returns Successor tree - type list with starting vertex as first item

    args:
    graph -- Tested graph - type networkx graph
    start_vertex -- Index of the vertex you want to find successor tree of - type int
    """
    S1=[]
    S2=[start_vertex]
    while S2:
        v=S2.pop()
        if v not in S1:
            S1.append(v)
            try:
                for w in nx.dfs_successors(graph,v)[v]:
                    S2.append(w)
            except KeyError:
                x=0 # do nothing
    return S1


def largest_successor_tree(graph):
    """
    Finds the largest successor tree in graph (our definition of component)

    Returns largest successor tree - type list

    args:
    graph -- graph to be tested - type networkx graph
    """
    list_of_trees=[]
    seen_vertices=[]
    for i in np.arange(nx.number_of_nodes(graph)):
        if i not in seen_vertices:
            x=complete_successor_tree(graph,i)
            list_of_trees.append(x)
            seen_vertices=seen_vertices + x
    largest_tree=list_of_trees[0]
    length_largest=len(largest_tree)
    for j in np.arange(1,len(list_of_trees)):
        tmp_length=len(list_of_trees[j])
        if tmp_length>length_largest:
            largest_tree=list_of_trees[j]
            length_largest=tmp_length
    return largest_tree


def make_RGG_HYP2D(radius=R,num_vertices=num_vertex):
    """
    Makes a graph with vertices connected if within hyperbolic radius of another

    Returns graph - type graph as networkx graph with xpos,time as node attributes

    args:
    radius -- hyperbolic radius in which nodes are connected if within - type float
    num_vertices -- the number of vertices in the graph - type int
    """
    G=nx.DiGraph()
    G.add_node(0, xpos=random.random(), ypos=random.random(), time=random.random())
    for i in np.arange(1,num_vertices):
        G.add_node(i, xpos=random.random(), ypos=random.random(), time=random.random())
        for j in np.arange(i-1):
            if pair_within_R(i, j, radius, G):
                first, second = temporal_order(i, j, G)
                G.add_edge(first, second)
    return G



def average_shortest_path_length(graph):
    """
    Returns the average of the shortest path between each pair of connected nodes - type float

    args:
    graph -- Graph to be tested - type networkx graph
    """
    total_path_length=0
    number_pairs=0
    for v in np.arange(graph.number_of_nodes()):
        succ_tree=complete_successor_tree(graph,v)[1:]
        if not succ_tree:
            x=0 #do nothing
        else:
            for w in succ_tree:
                total_path_length+=len(nx.shortest_path(graph,v,w))-1
                number_pairs+=1
    try:
        return np.float(total_path_length)/number_pairs
    except ZeroDivisionError:
        return 0.0


def generate_P_infty_plot(runs=20, num_vertex=500):
    """
    Generates the plot for the P_infinity phase transition.
    """
    radii=np.logspace(-3,-0.5,20)
    P_infty=np.empty(len(radii))
    counter=0
    for R in radii:
        tmp=[]
        for i in np.arange(runs):
            G=make_RGG_HYP2D(R,num_vertex)
            largest_tree=largest_successor_tree(G)
            tmp.append(np.float(len(largest_tree))/num_vertex)
        avg=sum(tmp)/float(len(tmp))
        P_infty[counter]=avg
        counter+=1
    NR2logR=[r*r*np.log(r)*num_vertex for r in radii]
    plt.plot(radii, P_infty, 'ro')
    plt.xscale('log')

def plot_ASP(runs, num_vertex):
    """
    ASP=average shortest path length
    Plots the average shortest path length

    """
    radii = np.logspace(-2.5, 0, 10)
    Path_Lens = np.empty(len(radii))
    counter = 0
    for R in radii:
        tmp = []
        for i in np.arange(runs):
            G = make_RGG_HYP2D(R, num_vertex)
            tmp.append(average_shortest_path_length(G))
        avg = sum(tmp) / np.float(len(tmp))
        Path_Lens[counter] = avg
        counter += 1
    plt.plot(radii, Path_Lens)



