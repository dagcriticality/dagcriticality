import numpy as np
import networkx as nx
import random
import matplotlib.pyplot as plt


def make_RGG_BOX2D(radius, num_vertices):
    """
    Creates a graph with nodes connected if within radius in box-space

    args:
    radius -- connection radius as float
    number_vertices -- number of vertices in graph as float
    """
    G = nx.DiGraph()
    G.add_node(0, x1=random.random(), x2=random.random(), x3=random.random())
    for i in np.arange(1, num_vertices):
        G.add_node(i, x1=random.random(), x2=random.random(), x3=random.random())
        for j in np.arange(i - 1):
            if pair_within_R(i, j, radius, G):
                first, second = temporal_order(i, j, G)
                G.add_edge(first, second)
    return G


def pair_within_R(vertex1_index, vertex2_index, radius, graph):
    """
    Tests whether two vertices are within connection radius

    Returns True if vertices are within connection radius, False otherwise

    args:
    vertex1_index, vertex2_index -- two vertex indices to be tested in type int
    radius -- connection radius in type float
    graph -- graph tested as networkx graph
    """
    x1_attribs = nx.get_node_attributes(graph, 'x1')
    x2_attribs = nx.get_node_attributes(graph, 'x2')
    x3_attribs = nx.get_node_attributes(graph, 'x3')
    x1diff = np.abs(x1_attribs[vertex1_index] - x1_attribs[vertex2_index])
    x2diff = np.abs(x2_attribs[vertex1_index] - x2_attribs[vertex2_index])
    x3diff = np.abs(x3_attribs[vertex1_index] - x3_attribs[vertex2_index])
    if x1diff < radius or x2diff < radius or x3diff < radius:
        return True
    else:
        return False


def temporal_order(vertex1_index, vertex2_index, graph):
    """
    Orders two vertices in terms of their time value.

    Returns the two vertex indices in order t1, t2 with t1<t2

    args:
    vertex1_index, vertex2_index -- vertex indices in the graph to be tested as int
    graph -- Graph in question in type Graph
    """
    x1_attribs = nx.get_node_attributes(graph, 'x1')
    x2_attribs = nx.get_node_attributes(graph, 'x2')
    x3_attribs = nx.get_node_attributes(graph, 'x3')
    time_vertex1 = x1_attribs[vertex1_index] ** 2 + x2_attribs[vertex1_index] ** 2 + x3_attribs[vertex1_index] ** 2
    time_vertex2 = x1_attribs[vertex2_index] ** 2 + x2_attribs[vertex2_index] ** 2 + x3_attribs[vertex2_index] ** 2
    if time_vertex1 < time_vertex2:
        return vertex1_index, vertex2_index
    else:
        return vertex2_index, vertex1_index


def complete_successor_tree(graph, start_vertex):
    """
    Generates the complete successor tree for a given vertex in the graph

    Returns Successor tree as list with starting vertex as first item

    args:
    graph -- Tested graph as type networkx graph
    start_vertex -- Index of the vertex you want to find successor tree of as int
    """
    S1 = []
    S2 = [start_vertex]
    while S2:
        v = S2.pop()
        if v not in S1:
            S1.append(v)
            try:
                for w in nx.dfs_successors(graph, v)[v]:
                    S2.append(w)
            except KeyError:
                x = 0  # do nothing
    return S1


def largest_successor_tree(graph):
    """
    Finds the largest successor tree in graph (our definition of component)

    Returns largest successor tree as list

    args:
    Graph as networkx graph
    """
    list_of_trees = []
    seen_vertices = []
    for i in np.arange(nx.number_of_nodes(graph)):
        if i not in seen_vertices:
            x = complete_successor_tree(graph, i)
            list_of_trees.append(x)
            seen_vertices = seen_vertices + x
    largest_tree = list_of_trees[0]
    length_largest = len(largest_tree)
    for j in np.arange(1, len(list_of_trees)):
        tmp_length = len(list_of_trees[j])
        if tmp_length > length_largest:
            largest_tree = list_of_trees[j]
            length_largest = tmp_length
    return largest_tree


def generate_P_infty_plot(runs=20, num_vertex=500, R_lower=-2, R_upper=-1, num_radii=10):
    """
    Generates the plot for the P_infinity phase transition when varying radius.
    List of radii will be logarithmically distributed

    args:
    runs -- Number of graphs averaged over for each input radius - type int
    num_vertex -- Number of vertices in each simulated graph - type int
    R_lower -- Lower bound of radii investigated in form 10^{R_lower} - type int
    R_upper -- Upper bound of radii investigated in form 10^{R_upper} - type int
    num_radii -- Number of radii P_infinity calculated for - type int
    """
    radii = np.logspace(R_lower, R_upper, num_radii)
    P_infty = np.empty(len(radii))
    counter = 0
    for R in radii:
        tmp = []
        for i in np.arange(runs):
            G = make_RGG_BOX1D(R, num_vertex)
            largest_tree = largest_successor_tree(G)
            tmp.append(np.float(len(largest_tree)) / num_vertex)
        avg = sum(tmp) / float(len(tmp))
        P_infty[counter] = avg
        counter += 1
    NR2 = [r * num_vertex for r in radii]
    plt.plot(radii, P_infty)
    plt.xlabel('R')
    plt.ylabel('P_infinity')