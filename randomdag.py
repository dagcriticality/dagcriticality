import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import numpy.random as rnd


def ertodag(N, p):
    """Create an Erdos Renyi random graph and produce a directed
    acyclic graph by ordering the nodes and directing the edges accordingly

    Args:
        N (int): The number of vertices in the graph
        p (float): The probability that any given possible edge will exist

    Returns:
        G (networkx.Digraph): A directed acyclic graph
    """
    erg = nx.erdos_renyi_graph(N, p)
    G = nx.DiGraph()
    G.add_nodes_from(erg.node)
    G.add_edges_from(erg.edges())
    print(nx.is_directed_acyclic_graph(G))
    return (G)

def add_distribution(a,b):
    """Pad out a smaller array with zeros on the right hand side and add
    to larger array

    Args:
        a (1d numpy.array): One array to be added
        b (1d numpy.array): The second array to be added

    Returns:
        a+b (1d numpy.array): The sum of the two arrays as long as
        the longest of a and b"""
    d = a.size-b.size
    if d==0:
        return a+b
    if d>0:
        return np.pad(b,(0,d),'constant')+a
    if d<0:
        return np.pad(a,(0,-d), 'constant')+b

def subtract_distribution(a,b):
    """Pad out a smaller array with zeros on the right hand side and subtract
    one array from another"

    Args:
        a (1d numpy.array): One array to be added
        b (1d numpy.array): The second array to be added

    Returns:
        a-b (1d numpy.array): The array a-b with length as long as
        the longest of a and b"""
    d = a.size-b.size
    if d==0:
        return a-b
    if d>0:
        return -np.pad(b,(0,d),'constant')+a
    if d<0:
        return np.pad(a,(0,-d), 'constant')-b


def indist(G):
    """Generate an array containing the in-degree distribution

    Args:
        G (networkx.DiGraph): The graph for which the in-degree distribution
        will be calculated

    Returns:
        dist (numpy.array): A numpy array containing the in-degree
        distribution
    """
    degrees = [G.in_degree(i) for i in G.nodes()]
    dist = np.bincount(degrees)
    return dist


def outdist(G):
    """Generate an array containing the out-degree distribution

    Args:
        G (networkx.DiGraph): The graph for which the out-degree distribution
        will be calculated

    Returns:
        dist (numpy.array): A numpy array containing the out-degree
        distribution
    """
    degrees = [G.out_degree(i) for i in G.nodes()]
    dist = np.bincount(degrees)
    return dist

def genrandomdag(N,m):
    # scatter m in stubs and m out stubs randomly among the vertices
    instubs = [rnd.randint(0,N-1) for i in range(m)]
    outstubs = []
    indegrees = np.bincount(instubs)
    outdegrees = np.zeros(N)
    usedstubs = 0
    for i in range(N):
        free_instubs = np.sum(indegrees[:i])# number of free in stubs before i
        if free_instubs:
            index = rnd.randint(min(free_instubs,m))# pick a number of outstubs less than this to drop
            outstubs += [rnd.randint(i,N) for j in range(index)]


    outdegrees = np.bincount(outstub)
    G = nx.DiGraph()
    pickarray = np.cumsum(indegrees)
    for i in range(N):
        G.add_node(i)
        for j in range(outdegrees[i]):
            print(i-1)
            print(pickarray[i-1])
            index = rnd.randint(pickarray[i-1])
            node = np.where(pickarray > index)[0][0] - 1
            G.add_edge(i,node)
            pickarray[node:] = pickarray[node:]-1
    return G

def plot_phasetrans(N = 100):
    p_base = 1/N
    pn_list = np.linspace(0.5,1.5,20)
    p_inf = []
    for i in pn_list:
        G = ertodag(N, i*p_base)
        sgcc = len(max(nx.connected_component_subgraphs(G)).nodes())
        p_inf.append(sgcc/N)
    plt.plot(pn_list, p_inf)
    return([pn_list, p_inf])




def swapforlambda(N,m):
    # scatter m in stubs and m out stubs randomly among the vertices
    instubs = [rnd.randint(0, N - 1) for i in range(m)]
    outstubs = [rnd.randint(1, N) for i in range(m)]
    indegrees = np.bincount(instubs)
    outdegrees = np.bincount(outstubs)
    cumindeg = np.cumsum(indegrees)
    cumoutdeg = np.cumsum(outdegrees)
    lam = subdists(cumindeg[:-1],cumoutdeg[1:])
    for i in range(N):
        free_instubs = np.sum(indegrees[:i])  # number of free in stubs before i
        if free_instubs:
            index = rnd.randint(min(free_instubs, m))  # pick a number of outstubs less than this to drop
            outstubs += [rnd.randint(i, N) for j in range(index)]




"""distin = np.array([])
distout = np.array([])

for i in range(100):
    G = ertodag(1000,0.01)
    distin = fitdists(distin, indist(G))
    distout = fitdists(distout, outdist(G))

plt.plot(distin/100)
plt.plot(distout/100)"""

#randomly place and sort