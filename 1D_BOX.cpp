#include <iostream>
#include <stdlib.h>
#include <boost/graph/adjacency_list.hpp>
#include <random>

using namespace std;
using namespace boost;


struct vertex_properties
    {
    float xpos;
    float ypos;
    };

typedef adjacency_list<vecS, vecS, directedS, vertex_properties> Graph;
// look up vecS compared to linked list sort of thing


bool test_pair_vertex(Graph::vertex_descriptor v1, Graph::vertex_descriptor v2, Graph g, float R){
    float distx = g[v1].xpos - g[v2].xpos;
    float disty = g[v1].ypos - g[v2].ypos;
    if(abs(distx)<R || abs(disty)<R){
        return true;
    }
    else{
        return false;
    }
}

bool temporal_order(Graph::vertex_descriptor v1, Graph::vertex_descriptor v2, Graph g){
    // returns true if v1 comes before v2, false otherwise
    float time1 = (g[v1].xpos*g[v1].xpos)+(g[v1].ypos*g[v1].ypos);
    float time2 = (g[v2].xpos*g[v2].xpos)+(g[v2].ypos*g[v2].ypos);
    if(time1<time2){
        return true;
    }
    else{
        return false;
    }

}

void add_a_node(Graph& g)
    {
    Graph::vertex_descriptor v = add_vertex(g);
    std::random_device rd; 
    std::mt19937 gen(rd()); 
    std::uniform_int_distribution<> dis(0, 1E5);
    float xr = dis(gen)/1E5;
    float yr = dis(gen)/1E5;
    g[v].xpos=xr;
    g[v].ypos=yr;
    typedef graph_traits<Graph>::vertex_iterator vertex_iter;
    std::pair<vertex_iter, vertex_iter> vp;
    for (vp = vertices(g); vp.first != vp.second-1; ++vp.first){
        // set condition to vp.second -1 to avoid self-loops
        if(test_pair_vertex(v,*vp.first, g, 0.2)){
            // set Radius to be 0.5 for now
            if(temporal_order(v,*vp.first,g)){
                std::pair<adjacency_list<>::edge_descriptor, bool> p;
                p=add_edge(v, *vp.first,g);
                cout << v << ", " << *vp.first << endl;
            }
            else{
                std::pair<adjacency_list<>::edge_descriptor, bool> q;
                q=add_edge(*vp.first,v,g);
                cout << *vp.first << ", " << v << endl;
            }

        }
    }
    }


int main(){


    Graph g;

    std::random_device rd; 
    std::mt19937 gen(rd()); 
    std::uniform_int_distribution<> dis(0, 1E5);
    float xrand = dis(gen)/1E5;
    float yrand = dis(gen)/1E5;


    Graph::vertex_descriptor v1 = add_vertex(g);

    g[v1].xpos = xrand;
    g[v1].ypos = yrand;

    for(int i=0; i<50; i++){ // here is where you define what N is to be
        add_a_node(g);
    }

    
    
    return 0;
}

